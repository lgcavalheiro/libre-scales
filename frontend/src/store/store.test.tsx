import { configureStore } from "@reduxjs/toolkit";
import { type ToolkitStore } from "@reduxjs/toolkit/dist/configureStore";
import {
  type RenderResult,
  render,
  screen,
  fireEvent,
  waitFor,
} from "@testing-library/react";
import { storeConfig } from ".";
import { Provider, useDispatch, useSelector } from "react-redux";
import {
  selectRawData,
  selectReportData,
  setRawData,
  setReportData,
} from "./features/reportSlice";

let fakeStore: ToolkitStore;

const preloadedState = {
  report: { rawData: [{ AG: 1 }], reportData: { generalInfo: { age: 1 } } },
};

const TestComponent = (): JSX.Element => {
  const dispatch = useDispatch();
  const rawData = useSelector(selectRawData);
  const report = useSelector(selectReportData);

  return (
    <>
      <p>{rawData[0].AG}</p>
      <p>{report.generalInfo.age}</p>
      <button
        onClick={() => {
          dispatch(setRawData([{ AG: 999 }]));
        }}
      >
        set raw data
      </button>
      <button
        onClick={() => {
          dispatch(setReportData({ generalInfo: { age: 888 } }));
        }}
      >
        set report data
      </button>
    </>
  );
};

const customRender = (preloadedState: any): RenderResult => {
  fakeStore = configureStore({
    ...storeConfig,
    preloadedState,
  });

  return render(
    <Provider store={fakeStore}>
      <TestComponent />
    </Provider>
  );
};

describe("Redux store tests", () => {
  it("Should be able to set raw data", async () => {
    customRender(preloadedState);

    const btn = screen.getByRole("button", { name: "set raw data" });
    fireEvent.click(btn);

    await waitFor(() => {
      expect(screen.getByText("999")).toBeVisible();
    });
  });

  it("Should be able to set report data", async () => {
    customRender(preloadedState);

    const btn = screen.getByRole("button", { name: "set report data" });
    fireEvent.click(btn);

    await waitFor(() => {
      expect(screen.getByText("888")).toBeVisible();
    });
  });
});
