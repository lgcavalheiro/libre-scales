import { type PaletteMode } from "@mui/material";
import { createSlice } from "@reduxjs/toolkit";

export interface ConfigState {
  config: {
    mode: string;
  };
}

export const configSlice = createSlice({
  name: "config",
  initialState: {
    mode: "dark",
  },
  reducers: {
    switchMode: (state) => {
      state.mode = state.mode === "dark" ? "light" : "dark";
    },
  },
});

export const selectMode = (state: ConfigState): PaletteMode =>
  state.config.mode as PaletteMode;

export const { switchMode } = configSlice.actions;
export default configSlice.reducer;
