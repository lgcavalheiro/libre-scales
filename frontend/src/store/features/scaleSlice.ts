import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { GetScaleImages } from "../../../wailsjs/go/scaleresolvers/ScaleResolver";

interface ScaleData {
  DATA: Record<number, string>;
  SYSTEM: Record<number, string>;
}

export interface ScaleState {
  scale: {
    scale: string;
    folder: string;
    scaleData: ScaleData;
    scalesInfo: Record<string, string>;
  };
}

export const scaleSlice = createSlice({
  name: "scale",
  initialState: {
    scale: "",
    folder: "",
    scaleData: {},
    scalesInfo: {},
  },
  reducers: {
    setScale: (state, action) => {
      state.scale = action.payload;
    },
    setFolder: (state, action) => {
      state.folder = action.payload;
    },
    setScaleData: (state, action) => {
      state.scaleData = action.payload;
    },
    resetState: (state) => {
      state.folder = "";
      state.scale = "";
      state.scaleData = {};
      state.scalesInfo = {};
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getInitialScalesInfo.fulfilled, (state, action) => {
      state.scalesInfo = action.payload;
    });

    builder.addCase(getInitialScalesInfo.rejected, (state, action) => {
      state.scalesInfo = {};
    });
  },
});

export const getInitialScalesInfo = createAsyncThunk(
  "getInitialScalesInfo",
  // eslint-disable-next-line @typescript-eslint/return-await
  async () => await GetScaleImages(),
);

export const selectScale = (state: ScaleState): string => state.scale.scale;
export const selectFolder = (state: ScaleState): string => state.scale.folder;
export const selectScaleData = (state: ScaleState): ScaleData =>
  state.scale.scaleData;
export const selectScalesInfo = (state: ScaleState): Record<string, string> =>
  state.scale.scalesInfo;

export const { setScale, setFolder, setScaleData, resetState } =
  scaleSlice.actions;
export default scaleSlice.reducer;
