import { createSlice } from "@reduxjs/toolkit";
import { type BC601Data } from "../../shared/parsers/tanita/bc601";

export interface BodyPart {
  percentage: number;
  mass: number;
}

export interface SpecializedInfo {
  date: string;
  globalPercentage: number;
  globalMass: number;
  torso: BodyPart;
  leftArm: BodyPart;
  rightArm: BodyPart;
  leftLeg: BodyPart;
  rightLeg: BodyPart;
}

export interface MiscInfo {
  weight: number;
  bmi: number;
  visceralFat: number;
  metabolicAge: number;
  bodyWater: number;
  boneMass: number;
  calorieIntake: number;
  date: string;
}

export interface GeneralInfo {
  age: number;
  gender: string;
  type: string;
  height: number;
  activityLevel: number;
}

export interface Report {
  generalInfo: GeneralInfo;
  miscInfo: MiscInfo[];
  fatInfo: SpecializedInfo[];
  muscleInfo: SpecializedInfo[];
}

interface ReportState {
  report: {
    rawData: BC601Data[];
    reportData: Report;
  };
}

export const reportSlice = createSlice({
  name: "report",
  initialState: {
    rawData: [],
    reportData: {},
  },
  reducers: {
    setRawData: (state, action) => {
      state.rawData = action.payload;
    },
    setReportData: (state, action) => {
      state.reportData = action.payload;
    },
  },
});

export const selectRawData = (state: ReportState): BC601Data[] =>
  state.report.rawData;
export const selectReportData = (state: ReportState): Report =>
  state.report.reportData;

export const { setRawData, setReportData } = reportSlice.actions;
export default reportSlice.reducer;
