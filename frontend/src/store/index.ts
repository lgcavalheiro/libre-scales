import { configureStore } from "@reduxjs/toolkit";
import configSlice from "./features/configSlice";
import scaleSlice from "./features/scaleSlice";
import reportSlice from "./features/reportSlice";

export const storeConfig = {
  reducer: {
    config: configSlice,
    scale: scaleSlice,
    report: reportSlice,
  },
};

export default configureStore(storeConfig);
