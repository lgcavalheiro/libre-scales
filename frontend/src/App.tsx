import { useEffect, useMemo } from "react";
import { Box, CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import { selectMode } from "./store/features/configSlice";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "./shared/Navbar";
import AppRoutes from "./shared/AppRoutes";
import { useTranslation } from "react-i18next";
import {
  getInitialScalesInfo,
  selectScalesInfo,
} from "./store/features/scaleSlice";

const App = (): JSX.Element => {
  const dispatch = useDispatch();
  const { i18n } = useTranslation();

  const mode = useSelector(selectMode);
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode]
  );

  const hasScalesInfo = Object.keys(useSelector(selectScalesInfo)).length > 0;

  useEffect(() => {
    const lang = navigator.language;
    void i18n.changeLanguage(lang);

    if (!hasScalesInfo) dispatch(getInitialScalesInfo() as any);
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Navbar />
      <Box
        sx={{ padding: "4px 32px", justifyContent: "center", display: "flex" }}
      >
        <Box sx={{ width: 1080, marginTop: 10 }}>
          <AppRoutes />
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export default App;
