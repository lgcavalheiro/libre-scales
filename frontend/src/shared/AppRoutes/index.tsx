import { Route, Routes } from "react-router-dom";
import Home from "../../views/Home";
import RegisterClient from "../../views/RegisterClient";
import ViewClients from "../../views/ViewClients";
import ReadScaleData from "../../views/ReadScaleData";
import ViewScaleData from "../../views/ViewScaleData";
import ViewReport from "../../views/ViewReport";

const AppRoutes = (): JSX.Element => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="client">
        <Route index path="register" element={<RegisterClient />} />
        <Route path="view" element={<ViewClients />} />
      </Route>
      <Route path="data">
        <Route index path="read" element={<ReadScaleData />} />
        <Route path="view" element={<ViewScaleData />} />
        <Route path="report" element={<ViewReport />} />
      </Route>
    </Routes>
  );
};

export default AppRoutes;
