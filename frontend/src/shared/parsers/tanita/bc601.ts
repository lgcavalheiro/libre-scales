import {
  type MiscInfo,
  type Report,
  type SpecializedInfo,
} from "../../../store/features/reportSlice";

type Literal = Record<string | number, string>;

const GENDERS: Literal = {
  2: "female",
  1: "male",
};

const LENGTH_UNITS: Literal = {
  2: "cm",
};

const MASS_UNITS: Literal = {
  2: "kg",
};

const BODY_TYPES: Literal = {
  0: "standard",
  1: "athlete", // assuming, could be 2
};

export interface BC601Data {
  AG: number;
  AL: number;
  Bt: string;
  CS: string;
  DT: string;
  FL: number;
  FR: number;
  FT: number;
  FW: number;
  Fl: number;
  Fr: number;
  GE: string;
  Hm: number;
  IF: number;
  MI: number;
  MO: string;
  Ti: string;
  Wk: number;
  bW: number;
  mL: number;
  mR: number;
  mT: number;
  mW: number;
  ml: number;
  mr: number;
  rA: number;
  rD: number;
  ww: number;
  "{0": number;
  "~0": string;
  "~1": string;
  "~2": number;
  "~3": number;
}

export interface BC601Profile {
  "{0": number;
  "~1": string;
  "~3": number;
  MO: string;
  DB: string;
  Bt: string; // TODO: research what this is (something to do with athlete mode, male athlete = 2, just male = 0)
  GE: string;
  Hm: number;
  AL: number;
  CS: string; // TODO: research what this is (apparently changes per entry)
}

const splitValues = (value: string): Record<string, string> => {
  value = value.split("\n")[0];

  const partial: Record<string, string> = {};

  const splitted = value.split(",");
  splitted.forEach((v, i) => {
    if (i % 2 === 0) {
      partial[v] = splitted[i + 1];
    }
  });

  return partial;
};

export const parseBc601Data = (value: string): BC601Data => {
  const partial = splitValues(value);

  return {
    "{0": Number(partial["{0"]),
    "~0": LENGTH_UNITS[Number(partial["~0"])],
    "~1": MASS_UNITS[Number(partial["~1"])],
    "~2": Number(partial["~2"]),
    "~3": Number(partial["~3"]),
    AG: Number(partial.AG),
    AL: Number(partial.AL),
    Bt: BODY_TYPES[Number(partial.Bt)],
    CS: partial.CS,
    DT: partial.DT.replaceAll('"', ""),
    FL: Number(partial.FL),
    FR: Number(partial.FR),
    FT: Number(partial.FT),
    FW: Number(partial.FW),
    Fl: Number(partial.Fl),
    Fr: Number(partial.Fr),
    GE: GENDERS[partial.GE],
    Hm: Number(partial.Hm),
    IF: Number(partial.IF),
    MI: Number(partial.MI),
    MO: partial.MO.replaceAll('"', ""),
    Ti: partial.Ti.replaceAll('"', ""),
    Wk: Number(partial.Wk),
    bW: Number(partial.bW),
    mL: Number(partial.mL),
    mR: Number(partial.mR),
    mT: Number(partial.mT),
    mW: Number(partial.mW),
    ml: Number(partial.ml),
    mr: Number(partial.mr),
    rA: Number(partial.rA),
    rD: Number(partial.rD),
    ww: Number(partial.ww),
  };
};

export const parseBc601Profile = (value: string): BC601Profile => {
  const partial = splitValues(value);

  return {
    "{0": Number(partial["{0"]),
    "~1": LENGTH_UNITS[Number(partial["~1"])],
    "~3": Number(partial["~3"]),
    MO: partial.MO.replaceAll('"', ""),
    DB: partial.DB.replaceAll('"', ""),
    Bt: BODY_TYPES[Number(partial.Bt)],
    GE: GENDERS[partial.GE],
    Hm: Number(partial.Hm),
    AL: Number(partial.AL),
    CS: partial.CS,
  };
};

const calculateFatEntry = (data: BC601Data): SpecializedInfo => {
  const fatMass = Number(((data.Wk * data.FW) / 100).toFixed(2));
  return {
    date: data.DT,
    globalPercentage: data.FW,
    globalMass: fatMass,
    torso: {
      percentage: data.FT,
      mass: Number(((fatMass * data.FT) / 100).toFixed(2)),
    },
    leftArm: {
      percentage: data.Fl,
      mass: Number(((fatMass * data.Fl) / 100).toFixed(2)),
    },
    rightArm: {
      percentage: data.Fr,
      mass: Number(((fatMass * data.Fr) / 100).toFixed(2)),
    },
    leftLeg: {
      percentage: data.FL,
      mass: Number(((fatMass * data.FL) / 100).toFixed(2)),
    },
    rightLeg: {
      percentage: data.FR,
      mass: Number(((fatMass * data.FR) / 100).toFixed(2)),
    },
  };
};

const calculateMuscleEntry = (data: BC601Data): SpecializedInfo => {
  const muscleMass = Number(((data.Wk * data.mW) / 100).toFixed(2));
  return {
    date: data.DT,
    globalPercentage: data.mW,
    globalMass: muscleMass,
    torso: {
      percentage: data.mT,
      mass: Number(((muscleMass * data.mT) / 100).toFixed(2)),
    },
    leftArm: {
      percentage: data.ml,
      mass: Number(((muscleMass * data.ml) / 100).toFixed(2)),
    },
    rightArm: {
      percentage: data.mr,
      mass: Number(((muscleMass * data.mr) / 100).toFixed(2)),
    },
    leftLeg: {
      percentage: data.mL,
      mass: Number(((muscleMass * data.mL) / 100).toFixed(2)),
    },
    rightLeg: {
      percentage: data.mR,
      mass: Number(((muscleMass * data.mR) / 100).toFixed(2)),
    },
  };
};

const calculateMiscEntry = (data: BC601Data): MiscInfo => ({
  bmi: data.MI,
  bodyWater: data.ww,
  boneMass: data.bW,
  calorieIntake: data.rD,
  metabolicAge: data.rA,
  visceralFat: data.IF,
  weight: data.Wk,
  date: data.DT,
});

export const parseBC601DataToReport = (rawData: BC601Data[]): Report => {
  const report: Report = {
    generalInfo: {
      age: rawData[0].AG,
      activityLevel: rawData[0].AL,
      gender: rawData[0].GE,
      height: rawData[0].Hm,
      type: rawData[0].Bt,
    },
    fatInfo: [],
    miscInfo: [],
    muscleInfo: [],
  };

  for (const data of rawData) {
    report.fatInfo.push(calculateFatEntry(data));
    report.muscleInfo.push(calculateMuscleEntry(data));
    report.miscInfo.push(calculateMiscEntry(data));
  }

  return report;
};
