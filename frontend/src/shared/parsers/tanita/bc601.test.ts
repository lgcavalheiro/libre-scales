import {
  type BC601Data,
  parseBC601DataToReport,
  parseBc601Data,
  parseBc601Profile,
} from "./bc601";

describe("BC-601 parsers", () => {
  it("Should be able to parse DATA", () => {
    const input =
      '{0,16,~0,2,~1,2,~2,3,~3,4,MO,"BC-601",DT,"02/05/2023",Ti,"11:59:02",Bt,0,GE,1,AG,30,Hm,171.0,AL,1,Wk,104.7,MI,35.8,FW,34.3,Fr,32.2,Fl,35.4,FR,31.8,FL,32.6,FT,35.9,mW,65.4,mr,4.0,ml,3.8,mR,11.9,mL,11.8,mT,33.9,bW,3.4,IF,15,rD,3245,rA,45,ww,46.0,CS,73';

    const result = parseBc601Data(input);

    expect(Object.keys(result).length).toBe(33);
  });

  it("Should be able to parse SYSTEM", () => {
    const input =
      '{0,16,~1,2,~3,4,MO,"BC-601",DB,"04/12/1992",Bt,0,GE,1,Hm,171.0,AL,1,CS,EA';

    const result = parseBc601Profile(input);

    expect(Object.keys(result).length).toBe(10);
  });

  it("Should parse BC601 data to report object", () => {
    const rawData: BC601Data[] = [
      {
        AG: 25,
        AL: 1,
        Bt: "standard",
        CS: "CS value",
        DT: "2021-01-01",
        FL: 1,
        FR: 2,
        FT: 3,
        FW: 4,
        Fl: 5,
        Fr: 6,
        GE: "male",
        Hm: 180,
        IF: 7,
        MI: 8,
        MO: "MO value",
        Ti: "Ti value",
        Wk: 9,
        bW: 10,
        mL: 11,
        mR: 12,
        mT: 13,
        mW: 14,
        ml: 15,
        mr: 16,
        rA: 17,
        rD: 18,
        ww: 19,
        "{0": 20,
        "~0": "Tilde 0 value",
        "~1": "Tilde 1 value",
        "~2": 21,
        "~3": 22,
      },
    ];

    const expectedReport = {
      generalInfo: {
        age: rawData[0].AG,
        activityLevel: rawData[0].AL,
        gender: rawData[0].GE,
        height: rawData[0].Hm,
        type: rawData[0].Bt,
      },
      fatInfo: [
        {
          date: "2021-01-01",
          globalMass: 0.36,
          globalPercentage: 4,
          leftArm: {
            mass: 0.02,
            percentage: 5,
          },
          leftLeg: {
            mass: 0,
            percentage: 1,
          },
          rightArm: {
            mass: 0.02,
            percentage: 6,
          },
          rightLeg: {
            mass: 0.01,
            percentage: 2,
          },
          torso: {
            mass: 0.01,
            percentage: 3,
          },
        },
      ],
      miscInfo: [
        {
          bmi: 8,
          bodyWater: 19,
          boneMass: 10,
          calorieIntake: 18,
          date: "2021-01-01",
          metabolicAge: 17,
          visceralFat: 7,
          weight: 9,
        },
      ],
      muscleInfo: [
        {
          date: "2021-01-01",
          globalMass: 1.26,
          globalPercentage: 14,
          leftArm: {
            mass: 0.19,
            percentage: 15,
          },
          leftLeg: {
            mass: 0.14,
            percentage: 11,
          },
          rightArm: {
            mass: 0.2,
            percentage: 16,
          },
          rightLeg: {
            mass: 0.15,
            percentage: 12,
          },
          torso: {
            mass: 0.16,
            percentage: 13,
          },
        },
      ],
    };

    const report = parseBC601DataToReport(rawData);

    expect(report).toEqual(expectedReport);
  });
});
