import {
  screen,
  render,
  fireEvent,
  type RenderResult,
} from "@testing-library/react";
import Navbar from ".";
import { type ReactNode } from "react";
import { Provider } from "react-redux";
import store from "../../store";
import { HashRouter } from "react-router-dom";

const mockedChangeLanguage = jest.fn();

jest.mock("react-i18next", () => ({
  useTranslation: () => ({
    t: (key: string) => key,
    i18n: {
      changeLanguage: mockedChangeLanguage,
    },
  }),
}));

const customRender = (ui: ReactNode): RenderResult =>
  render(
    <Provider store={store}>
      <HashRouter>{ui}</HashRouter>
    </Provider>
  );

describe("<Navbar />", () => {
  it("Should render", () => {
    customRender(<Navbar />);

    expect(screen.getByText("Libre Scales")).toBeInTheDocument();
    expect(screen.getByTestId("lang-btn")).toBeInTheDocument();
    expect(screen.getByTestId("mode-btn")).toBeInTheDocument();
  });

  it("Should switch modes on button click", () => {
    customRender(<Navbar />);

    expect(screen.getByTestId("dark-icon")).toBeInTheDocument();

    const btn = screen.getByTestId("mode-btn");
    fireEvent.click(btn);
    expect(screen.getByTestId("light-icon")).toBeInTheDocument();

    fireEvent.click(btn);
    expect(screen.getByTestId("dark-icon")).toBeInTheDocument();
  });

  it("Should switch languages on button click", () => {
    customRender(<Navbar />);

    const btn = screen.getByTestId("lang-btn");
    fireEvent.click(btn);

    const enUsBtn = screen.getByText("lang.en-US");
    fireEvent.click(enUsBtn);

    expect(mockedChangeLanguage).toHaveBeenCalledTimes(1);
    expect(mockedChangeLanguage).toHaveBeenCalledWith("en-US");
  });
});
