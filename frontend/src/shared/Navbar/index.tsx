import {
  Typography,
  Toolbar,
  AppBar,
  Box,
  IconButton,
  Menu,
  MenuItem,
  ButtonBase,
} from "@mui/material";
import ScaleIcon from "@mui/icons-material/Scale";
import LightModeIcon from "@mui/icons-material/LightMode";
import Brightness2Icon from "@mui/icons-material/Brightness2";
import LanguageIcon from "@mui/icons-material/Language";
import { useDispatch, useSelector } from "react-redux";
import {
  selectMode,
  switchMode,
} from "../../store/features/configSlice";
import { type MouseEvent, useState } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { knownLanguages } from "../../i18n/locales";

const Navbar = (): JSX.Element => {
  const dispatch = useDispatch();
  const mode = useSelector(selectMode);
  const { t, i18n } = useTranslation();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: MouseEvent<HTMLButtonElement>): void => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (): void => {
    setAnchorEl(null);
  };
  const handleChangeLang = (lang: string): void => {
    void i18n.changeLanguage(lang);
    handleClose();
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <ButtonBase component={Link} to="/" sx={{ flexGrow: 1 }}>
            <ScaleIcon sx={{ mr: 1 }} />
            <Typography variant="h6" component="div">
              Libre Scales
            </Typography>
          </ButtonBase>

          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            aria-controls={open ? "basic-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
            sx={{ mr: 2 }}
            data-testid="lang-btn"
          >
            <LanguageIcon />
          </IconButton>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            {knownLanguages.map((lang: string) => (
              <MenuItem
                onClick={() => {
                  handleChangeLang(lang);
                }}
                key={lang}
              >
                {t(`lang.${lang}`)}
              </MenuItem>
            ))}
          </Menu>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={() => dispatch(switchMode())}
            data-testid="mode-btn"
          >
            {mode === "dark" ? (
              <Brightness2Icon data-testid="dark-icon" />
            ) : (
              <LightModeIcon data-testid="light-icon" />
            )}
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
