import {
  DataGrid,
  type GridRowSelectionModel,
  type GridColDef,
  type GridRowId,
} from "@mui/x-data-grid";
import { useDispatch, useSelector } from "react-redux";
import {
  resetState,
  selectFolder,
  selectScaleData,
} from "../../store/features/scaleSlice";
import { useEffect, useMemo, useState } from "react";
import {
  parseBC601DataToReport,
  parseBc601Data,
  parseBc601Profile,
} from "../../shared/parsers/tanita/bc601";
import { Box, Button, Paper, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { setRawData, setReportData } from "../../store/features/reportSlice";

const ViewScaleData = (): JSX.Element => {
  const dispatch = useDispatch();
  const scaleData = useSelector(selectScaleData);
  const folder = useSelector(selectFolder);
  const { t } = useTranslation();
  const [selectedProfile, setSelectedProfile] = useState<GridRowId>();
  const [selectedData, setSelectedData] = useState<GridRowSelectionModel>([]);

  const dataColumns: GridColDef[] = [
    {
      field: "id",
      headerName: t("view-scale-data.data-columns.id"),
      width: 90,
    },
    {
      field: "date",
      headerName: t("view-scale-data.data-columns.date"),
      width: 150,
    },
    {
      field: "weight",
      headerName: t("view-scale-data.data-columns.weight"),
      width: 150,
    },
    {
      field: "bodyFat",
      headerName: t("view-scale-data.data-columns.body-fat"),
      width: 150,
    },
    {
      field: "muscleMass",
      headerName: t("view-scale-data.data-columns.muscle-mass"),
      width: 150,
    },
    {
      field: "visceralFat",
      headerName: t("view-scale-data.data-columns.visceral-fat"),
      width: 150,
    },
  ];

  const profileColumns: GridColDef[] = [
    {
      field: "id",
      headerName: t("view-scale-data.profile-columns.id"),
      width: 90,
    },
    {
      field: "dateOfBirth",
      headerName: t("view-scale-data.profile-columns.date-of-birth"),
      width: 150,
    },
    {
      field: "gender",
      headerName: t("view-scale-data.profile-columns.gender"),
      width: 150,
    },
    {
      field: "height",
      headerName: t("view-scale-data.profile-columns.height"),
      width: 150,
    },
    {
      field: "bodyType",
      headerName: t("view-scale-data.profile-columns.body-type"),
      width: 150,
    },
    {
      field: "activityLevel",
      headerName: t("view-scale-data.profile-columns.activity-level"),
      width: 150,
    },
  ];

  const profileRows = useMemo(() => {
    const parsed = Object.entries(scaleData.SYSTEM).map(([_, val]) =>
      parseBc601Profile(val)
    );
    return parsed.map((entry, i) => ({
      id: i + 1,
      dateOfBirth: entry.DB,
      gender: t(`view-scale-data.${entry.GE}`),
      height: `${entry.Hm} ${entry["~1"]}`,
      bodyType: t(`view-scale-data.${entry.Bt}`),
      activityLevel: entry.AL,
    }));
  }, [scaleData, t]);

  const dataRows = useMemo(
    () =>
      Object.entries(scaleData.DATA).map(([_, data]) => {
        const grouped = data.split("\r\n").map((s) => parseBc601Data(s));
        return grouped.map((group, i) => ({
          id: i + 1,
          date: group.DT,
          weight: `${group.Wk} ${group["~1"]}`,
          bodyFat: group.FW,
          muscleMass: group.mW,
          visceralFat: group.IF,
        }));
      }),
    [scaleData]
  );

  const selectedDataIndex = useMemo(() => {
    const partial = Number(selectedProfile?.toString()) - 1;
    return !Number.isNaN(partial) ? partial : 0;
  }, [selectedProfile]);

  useEffect(() => {
    setSelectedData([]);
  }, [selectedProfile]);

  const handleClose = (): void => {
    dispatch(resetState);
    window.location.hash = "#";
  };

  const handleSelectScaleOrFolder = (): void => {
    dispatch(resetState);
    window.location.hash = "#/data/read";
  };

  const handleViewReport = (): void => {
    const newRawData = scaleData.DATA[selectedDataIndex + 1]
      .split("\r\n")
      .map((s) => parseBc601Data(s));
    dispatch(setRawData(newRawData));
    dispatch(setReportData(parseBC601DataToReport(newRawData)))
    window.location.hash = "#/data/report";
  };

  return (
    <Paper
      sx={{
        height: "fit-content",
        padding: 8,
        display: "flex",
        gap: 4,
      }}
    >
      <Box sx={{ display: "grid", gap: 6 }}>
        <Box>
          <Typography variant="h5">
            {t("view-scale-data.found-folder", { folder })}
          </Typography>
          <Box sx={{ display: "grid", gap: 4, gridTemplateColumns: "80% 1fr" }}>
            <DataGrid
              rows={profileRows}
              columns={profileColumns}
              onRowSelectionModelChange={([newRow]) => {
                setSelectedProfile(newRow);
              }}
              rowSelectionModel={selectedProfile}
            />
            <Box sx={{ display: "flex", gap: 4, flexDirection: "column" }}>
              <Button
                variant="contained"
                sx={{ width: "100%", height: "fit-content" }}
                onClick={handleSelectScaleOrFolder}
              >
                {t("view-scale-data.change-scale-folder")}
              </Button>
              <Button
                variant="contained"
                sx={{ width: "100%", height: "fit-content" }}
                disabled={selectedProfile == null}
                onClick={handleViewReport}
              >
                {t("view-scale-data.view-report")}
              </Button>
            </Box>
          </Box>
        </Box>

        <Box>
          {selectedProfile != null && (
            <Typography variant="h5">
              {t("view-scale-data.found-data", { selectedProfile })}
            </Typography>
          )}
          <Box sx={{ display: "grid", gap: 4, gridTemplateColumns: "80% 1fr" }}>
            {selectedProfile != null ? (
              <DataGrid
                rows={dataRows[selectedDataIndex]}
                columns={dataColumns}
                onRowSelectionModelChange={(newRow) => {
                  setSelectedData(newRow);
                }}
                rowSelectionModel={selectedData}
                checkboxSelection
              />
            ) : (
              <Paper variant="outlined" sx={{ padding: 2 }}>
                <Typography variant="h6">
                  {t("view-scale-data.no-data")}
                </Typography>
              </Paper>
            )}
            <Box sx={{ display: "grid", gap: 2 }}>
              <Button
                variant="contained"
                color="success"
                sx={{ width: "100%", height: "fit-content" }}
                disabled={selectedData.length === 0}
              >
                {t("view-scale-data.save-selected")}
              </Button>
              <Button
                variant="contained"
                color="error"
                sx={{ placeSelf: "end", width: "100%", height: "fit-content" }}
                onClick={handleClose}
              >
                {t("view-scale-data.close")}
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Paper>
  );
};

export default ViewScaleData;
