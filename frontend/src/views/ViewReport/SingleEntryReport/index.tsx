import {
  Paper,
  Divider,
  Box,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import MiscData from "./components/MiscData";
import SpecializedData from "./components/SpecializedData";
import type {
  GeneralInfo,
  MiscInfo,
  SpecializedInfo,
} from "../../../store/features/reportSlice";

interface SingleLineReportProps {
  fatInfo: SpecializedInfo;
  generalInfo: GeneralInfo;
  miscInfo: MiscInfo;
  muscleInfo: SpecializedInfo;
}

export default function SingleEntryReport({
  fatInfo,
  generalInfo,
  miscInfo,
  muscleInfo,
}: SingleLineReportProps): JSX.Element {
  const { t } = useTranslation();

  return (
    <Paper elevation={1} sx={{ padding: 8, gap: 4, display: "grid" }}>
      <Divider>{t("report.general-info.summary")}</Divider>
      <Box
        id="info-container"
        sx={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", gap: 2 }}
      >
        <List
          component={Paper}
          elevation={2}
          sx={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)" }}
        >
          {Object.entries(generalInfo).map(([key, value]) => (
            <ListItem key={key}>
              <ListItemText
                primary={
                  <Typography variant="h5">
                    {t(`report.general-info.${key}`)}
                  </Typography>
                }
                secondary={value}
              />
            </ListItem>
          ))}
        </List>
        <MiscData data={miscInfo} />
      </Box>
      <SpecializedData
        title={t("report.specialized-info.fat-title")}
        percentageTitle={t("report.specialized-info.global-fat-p")}
        massTitle={t("report.specialized-info.global-fat-m")}
        data={fatInfo}
      />
      <SpecializedData
        title={t("report.specialized-info.muscle-title")}
        percentageTitle={t("report.specialized-info.global-muscle-p")}
        massTitle={t("report.specialized-info.global-muscle-m")}
        data={muscleInfo}
      />
    </Paper>
  );
}
