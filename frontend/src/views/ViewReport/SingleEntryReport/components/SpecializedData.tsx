import {
  Divider,
  List,
  Paper,
  ListItem,
  ListItemText,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@mui/material";
import { type SpecializedInfo } from "../../../../store/features/reportSlice";
import { useTranslation } from "react-i18next";

interface SpecializedDataProps {
  title: string;
  percentageTitle: string;
  massTitle: string;
  data: SpecializedInfo;
}

const SpecializedData = (props: SpecializedDataProps): JSX.Element => {
  const { t } = useTranslation();

  const { title, percentageTitle, massTitle, data } = props;

  return (
    <>
      <Divider>{title}</Divider>
      <List component={Paper} elevation={2} sx={{ display: "flex" }}>
        <ListItem>
          <ListItemText
            primary={<Typography variant="h5">{percentageTitle}</Typography>}
            secondary={data.globalPercentage}
          />
        </ListItem>
        <ListItem>
          <ListItemText
            primary={<Typography variant="h5">{massTitle}</Typography>}
            secondary={data.globalMass}
          />
        </ListItem>
      </List>
      <TableContainer component={Paper} elevation={2}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>{t("report.specialized-info.percentage")}</TableCell>
              <TableCell>{t("report.specialized-info.mass")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Object.entries(data).map(([key, value]) => {
              if (!["date", "globalPercentage", "globalMass"].includes(key))
                return (
                  <TableRow key={key}>
                    <TableCell>{t(`report.specialized-info.${key}`)}</TableCell>
                    <TableCell>{value.percentage}</TableCell>
                    <TableCell>{value.mass}</TableCell>
                  </TableRow>
                );
              return null;
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default SpecializedData;
