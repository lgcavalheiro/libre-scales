import { List, Paper, ListItem, ListItemText, Typography } from "@mui/material";
import { type MiscInfo } from "../../../../store/features/reportSlice";
import { useTranslation } from "react-i18next";

interface MiscDataProps {
  data: MiscInfo;
}

const MiscData = (props: MiscDataProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <List
      component={Paper}
      elevation={2}
      sx={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)" }}
    >
      {Object.entries(props.data).map(([key, value]) => (
        <ListItem key={key}>
          <ListItemText
            primary={
              <Typography variant="h5">
                {t(`report.misc-info.${key}`)}
              </Typography>
            }
            secondary={value}
          />
        </ListItem>
      ))}
    </List>
  );
};

export default MiscData;
