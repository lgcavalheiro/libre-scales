import { useSelector } from "react-redux";
import { selectReportData } from "../../store/features/reportSlice";
import { useMemo } from "react";
import MultiEntryReport from "./MultiEntryReport";
import SingleEntryReport from "./SingleEntryReport";

const ViewReport = (): JSX.Element => {
  const { fatInfo, generalInfo, miscInfo, muscleInfo } =
    useSelector(selectReportData);

  const isMultiline = useMemo(
    () => fatInfo.length + miscInfo.length + muscleInfo.length > 3,
    [fatInfo, miscInfo, muscleInfo]
  );

  if (isMultiline)
    return (
      <MultiEntryReport
        fatInfo={fatInfo}
        generalInfo={generalInfo}
        miscInfo={miscInfo}
        muscleInfo={muscleInfo}
      />
    );

  return (
    <SingleEntryReport
      fatInfo={fatInfo[0]}
      generalInfo={generalInfo}
      miscInfo={miscInfo[0]}
      muscleInfo={muscleInfo[0]}
    />
  );
};

export default ViewReport;
