import { Box, Divider, Paper, Typography } from "@mui/material";
import type { GeneralInfo } from "../../../../store/features/reportSlice";
import { useTranslation } from "react-i18next";

interface MultiEntryGeneralInfoProps {
  generalInfo: GeneralInfo;
  entryDates: string[];
}

export default function MultiEntryGeneralInfo({
  generalInfo,
  entryDates,
}: MultiEntryGeneralInfoProps): JSX.Element {
  const { t } = useTranslation();

  return (
    <Paper sx={{ display: "grid", gap: "16px", padding: ["16px", "32px"] }}>
      <Divider>
        <Typography fontWeight="500" fontSize="0.875rem">
          {t("report.multi-entry-general-info.general-information")}
        </Typography>
      </Divider>
      <Box sx={{ display: "grid", gridTemplateColumns: "1fr 1fr" }}>
        {Object.entries(generalInfo).map(([key, value]) => {
          return (
            <Typography key={key}>
              {t(`common.${key}`)}:{" "}
              {typeof value === "string" ? t(`common.${value}`) : value}
            </Typography>
          );
        })}
      </Box>
      <Typography variant="subtitle2">
        {t("report.multi-entry-general-info.viewing-entries", {
          entryCount: entryDates.length,
          startDate: entryDates[0],
          endDate: entryDates[entryDates.length - 1],
        })}
      </Typography>
    </Paper>
  );
}
