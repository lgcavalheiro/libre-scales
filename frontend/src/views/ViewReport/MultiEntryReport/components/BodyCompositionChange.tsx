import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControlLabel,
  FormGroup,
  IconButton,
  InputLabel,
  MenuItem,
  Modal,
  Paper,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { LineChart, type LineSeriesType } from "@mui/x-charts";
import { useSelector } from "react-redux";
import {
  type MiscInfo,
  type SpecializedInfo,
  type BodyPart,
  selectReportData,
} from "../../../../store/features/reportSlice";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { type MakeOptional } from "@mui/x-charts/models/helpers";
import EditIcon from "@mui/icons-material/Edit";
import DoneIcon from "@mui/icons-material/Done";
import CancelIcon from "@mui/icons-material/Cancel";
import { useTranslation } from "react-i18next";

const infoSections = ["fatInfo", "miscInfo", "muscleInfo"] as const;
type InfoSection = (typeof infoSections)[number];
type MeasurementType = "percentage" | "mass";
type ReportKey = keyof MiscInfo | keyof SpecializedInfo;

const SpecilizedInfoKeys: ReportKey[] = [
  "globalPercentage",
  "globalMass",
  "torso",
  "leftArm",
  "rightArm",
  "leftLeg",
  "rightLeg",
];
const MiscInfoKeys: ReportKey[] = [
  "weight",
  "bmi",
  "visceralFat",
  "metabolicAge",
  "bodyWater",
  "boneMass",
  "calorieIntake",
];

interface BodyCompositionChangeProps {
  defaultTitle?: string;
}

const isMiscInfo = (info: MiscInfo | SpecializedInfo): info is MiscInfo =>
  "weight" in info;

const isSpecInfo = (
  info: MiscInfo | SpecializedInfo
): info is SpecializedInfo => "globalMass" in info;

const isBodyPart = (info: string | number | BodyPart): info is BodyPart =>
  !["number", "string"].includes(typeof info) && "mass" in (info as any);

export default function BodyCompositionChange({
  defaultTitle,
}: BodyCompositionChangeProps): JSX.Element {
  const { t, i18n } = useTranslation();
  const report = useSelector(selectReportData);

  const [showModal, setShowModal] = useState(false);
  const [title, setTitle] = useState(
    defaultTitle ?? t("report.body-composition-change.default-title")
  );
  const [reportSection, setReportSection] = useState<InfoSection>(
    infoSections[0]
  );
  const [measurementType, setMeasurementType] =
    useState<MeasurementType>("percentage");
  const [reportKeys, setReportKeys] = useState<ReportKey[]>(
    reportSection === "miscInfo" ? MiscInfoKeys : SpecilizedInfoKeys
  );
  const [selectedReportKeys, setSelectedReportKeys] =
    useState<ReportKey[]>(reportKeys);
  const [series, setSeries] = useState<any>([]);

  const titleInputRef = useRef<HTMLInputElement>();

  const selectedInfo = useMemo(
    () => report[reportSection],
    [report, reportSection]
  );

  const filterMeasurementType = useCallback(
    (key: string): string => {
      if (["fatInfo", "muscleInfo"].includes(reportSection))
        return key.toLocaleLowerCase().includes(measurementType) ||
          key.length <= 8
          ? key
          : "filterout";
      return key;
    },
    [reportSection, measurementType]
  );

  const makeSeries = (
    newReportKeys: ReportKey[]
  ): Array<{
    label: string;
    data: Array<string | number | undefined>;
  }> =>
    newReportKeys
      .map((key) => ({
        label: t(`common.${filterMeasurementType(key)}`),
        data: selectedInfo.map((info) => {
          let infoData;

          if (isSpecInfo(info)) {
            infoData = info[key as keyof SpecializedInfo];
            if (isBodyPart(infoData)) return infoData[measurementType];
          }
          if (isMiscInfo(info)) infoData = info[key as keyof MiscInfo];

          return infoData;
        }),
      }))
      .filter((info) => info.label !== "filterout");

  useEffect(() => {
    let newReportKeys =
      reportSection === "miscInfo" ? MiscInfoKeys : SpecilizedInfoKeys;
    newReportKeys = newReportKeys
      .map((key) => filterMeasurementType(key))
      .filter((key) => key !== "filterout") as ReportKey[];

    setSelectedReportKeys(newReportKeys);
    setReportKeys(newReportKeys);
  }, [reportSection, measurementType]);

  useEffect(() => {
    const newSeries = makeSeries(selectedReportKeys);
    setSeries(newSeries);
  }, [selectedReportKeys, i18n.language]);

  const onClick = (): void => {
    setShowModal(!showModal);
  };

  const closeModal = (reason: "save" | "discard"): void => {
    if (reason === "save") setTitle(titleInputRef?.current?.value as string);
    setShowModal(false);
  };

  return (
    <Paper sx={{ display: "grid", gap: "16px", padding: ["16px", "32px"] }}>
      <Modal
        open={showModal}
        onClose={() => {
          closeModal("discard");
        }}
      >
        <Box
          sx={{
            minWidth: 400,
            bgcolor: "background.paper",
            border: "2px solid #000",
            position: "absolute" as "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            borderRadius: "4px",
            display: "grid",
            gap: "16px",
            padding: "16px",
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography fontSize={24}>
              {t("report.body-composition-change.component-settings")}
            </Typography>
            <Box sx={{ display: "flex" }}>
              <IconButton
                data-testid="save-btn"
                onClick={() => {
                  closeModal("save");
                }}
              >
                <DoneIcon />
              </IconButton>
              <IconButton
                data-testid="close-btn"
                onClick={() => {
                  closeModal("discard");
                }}
              >
                <CancelIcon />
              </IconButton>
            </Box>
          </Box>
          <Box
            sx={{
              display: "grid",
              gridTemplateColumns: "repeat(2, 1fr)",
              gap: "32px",
            }}
          >
            <Box sx={{ display: "grid", gap: "30px" }}>
              <TextField
                name="title-input"
                id="title-input"
                label="Title"
                variant="standard"
                defaultValue={title}
                inputRef={titleInputRef}
              />
              <InputLabel variant="standard" htmlFor="report-section-label">
                {t("report.body-composition-change.report-section")}
              </InputLabel>
              <Select
                variant="standard"
                labelId="report-section-label"
                id="report-section-input"
                defaultValue={reportSection}
                onChange={(e) => {
                  const newReportSection = e.target.value as InfoSection;
                  setReportSection(newReportSection);
                }}
              >
                {infoSections.map((section) => (
                  <MenuItem key={section} value={section}>
                    {t(`report.body-composition-change.${section}`)}
                  </MenuItem>
                ))}
              </Select>
              <InputLabel variant="standard" htmlFor="measurement-type-label">
                {t("report.body-composition-change.measurement-type")}
              </InputLabel>
              <Select
                variant="standard"
                labelId="measurement-type-label"
                id="measurement-type-input"
                onChange={(e) => {
                  setMeasurementType(e.target.value as MeasurementType);
                }}
                defaultValue={measurementType}
                disabled={reportSection === "miscInfo"}
              >
                <MenuItem value="mass">{t("common.mass")}</MenuItem>
                <MenuItem value="percentage">{t("common.percentage")}</MenuItem>
              </Select>
            </Box>
            <FormGroup>
              <Typography fontSize="20px">
                {t("report.body-composition-change.data-to-view")}
              </Typography>
              {reportKeys.map((key) => (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={selectedReportKeys.includes(key)}
                      onClick={() => {
                        if (selectedReportKeys.includes(key)) {
                          const newKeys = selectedReportKeys.filter(
                            (selectedKey) => selectedKey !== key
                          );
                          setSelectedReportKeys(newKeys);
                        } else {
                          const newKeys = [...selectedReportKeys, key];
                          setSelectedReportKeys(newKeys);
                        }
                      }}
                    />
                  }
                  label={t(`common.${key}`)}
                  key={key}
                />
              ))}
            </FormGroup>
          </Box>
        </Box>
      </Modal>
      <Divider>
        <Button startIcon={<EditIcon />} color="inherit" onClick={onClick}>
          {title}
        </Button>
      </Divider>
      <LineChart
        xAxis={[
          { scaleType: "point", data: selectedInfo.map((info) => info.date) },
        ]}
        series={
          series as unknown as Array<MakeOptional<LineSeriesType, "type">>
        }
        height={300}
      />
    </Paper>
  );
}
