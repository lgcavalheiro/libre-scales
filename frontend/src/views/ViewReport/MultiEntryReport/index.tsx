import { useMemo } from "react";
import type {
  SpecializedInfo,
  GeneralInfo,
  MiscInfo,
} from "../../../store/features/reportSlice";
import BodyCompositionChange from "./components/BodyCompositionChange";
import MultiEntryGeneralInfo from "./components/MultiEntryGeneralInfo";

interface MultiEntryReportProps {
  fatInfo: SpecializedInfo[];
  generalInfo: GeneralInfo;
  miscInfo: MiscInfo[];
  muscleInfo: SpecializedInfo[];
}

export default function MultiEntryReport({
  fatInfo,
  generalInfo,
}: MultiEntryReportProps): JSX.Element {
  const entryDates = useMemo(
    () => fatInfo.map((info) => info.date).sort(),
    [fatInfo]
  );

  return (
    <>
      <MultiEntryGeneralInfo
        generalInfo={generalInfo}
        entryDates={entryDates}
      />
      <br />
      <BodyCompositionChange />
    </>
  );
}
