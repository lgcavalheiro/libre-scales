import {
  type RenderResult,
  render,
  screen,
  fireEvent,
} from "@testing-library/react";
import { Provider } from "react-redux";
import { storeConfig } from "../../store";
import ViewReport from "./index";
import { configureStore } from "@reduxjs/toolkit";
import { type ReactNode } from "react";
import { type ToolkitStore } from "@reduxjs/toolkit/dist/configureStore";
import {
  preloadedMultiEntryState,
  preloadedSingleEntryState,
} from "../../../__mocks__/reportStateMocks";

let fakeStore: ToolkitStore;

const customRender = (ui: ReactNode, preloadedState: any): RenderResult => {
  fakeStore = configureStore({
    ...storeConfig,
    preloadedState,
  });

  return render(<Provider store={fakeStore}>{ui}</Provider>);
};

describe("<ViewReport />", () => {
  it("Should render a single entry report", () => {
    customRender(<ViewReport />, preloadedSingleEntryState);

    const summaryElement = screen.getByText("report.general-info.summary");
    const generalInfoElement = screen.getByText("report.general-info.age");

    expect(summaryElement).toBeInTheDocument();
    expect(generalInfoElement).toBeInTheDocument();
  });

  it("Should render a multi entry report", () => {
    customRender(<ViewReport />, preloadedMultiEntryState);

    const multiEntryGeneralInfoTitle = screen.getByText(
      "report.multi-entry-general-info.general-information"
    );
    const bodyCompositionChangeTitle = screen.getByText(
      "report.body-composition-change.default-title"
    );
    expect(bodyCompositionChangeTitle).toBeInTheDocument();
    expect(multiEntryGeneralInfoTitle).toBeInTheDocument();
  });

  it("Should open modal if BodyCompositionChange title is clicked", () => {
    customRender(<ViewReport />, preloadedMultiEntryState);

    const bodyCompositionChangeTitle = screen.getByText(
      "report.body-composition-change.default-title"
    );
    fireEvent.click(bodyCompositionChangeTitle);

    expect(
      screen.getByText("report.body-composition-change.component-settings")
    ).toBeVisible();
  });

  it.each([
    ["discard", "close"],
    ["save", "save"],
  ])(
    "Should %p changes if BodyCompositionChange's %p button is clicked",
    (mode, buttonType) => {
      const titleChange = "CHANGED TITLE";
      customRender(<ViewReport />, preloadedMultiEntryState);

      const bodyCompositionChangeTitle = screen.getByText(
        "report.body-composition-change.default-title"
      );
      fireEvent.click(bodyCompositionChangeTitle);

      const titleInput = screen.getByLabelText(/^Title/i);
      fireEvent.change(titleInput, { target: { value: titleChange } });

      const closeBtn = screen.getByTestId(`${buttonType}-btn`);
      fireEvent.click(closeBtn);

      expect(
        screen.getByText(
          mode === "discard"
            ? "report.body-composition-change.default-title"
            : titleChange
        )
      ).toBeVisible();
    }
  );

  it("Should discard changes if user clicks outside BodyCompositionChange's modal", () => {
    const titleChange = "CHANGED TITLE";
    customRender(<ViewReport />, preloadedMultiEntryState);

    const bodyCompositionChangeTitle = screen.getByText(
      "report.body-composition-change.default-title"
    );
    fireEvent.click(bodyCompositionChangeTitle);

    const titleInput = screen.getByLabelText(/^Title/i);
    fireEvent.change(titleInput, { target: { value: titleChange } });
    fireEvent.click(bodyCompositionChangeTitle);

    expect(
      screen.getByText("report.body-composition-change.default-title")
    ).toBeVisible();
    expect(titleInput).not.toBeInTheDocument();
  });
});
