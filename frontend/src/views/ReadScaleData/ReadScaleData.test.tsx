import {
  screen,
  render,
  type RenderResult,
  fireEvent,
  waitFor,
} from "@testing-library/react";
import ReadScaleData from ".";
import { type ReactNode } from "react";
import { Provider } from "react-redux";
import { HashRouter } from "react-router-dom";
import { configureStore } from "@reduxjs/toolkit";
import { storeConfig } from "../../store";
import { type ToolkitStore } from "@reduxjs/toolkit/dist/configureStore";
import { ReadFolder } from "../../../wailsjs/go/main/App";

let fakeStore: ToolkitStore;

const customRender = (ui: ReactNode): RenderResult => {
  fakeStore = configureStore({
    ...storeConfig,
    preloadedState: {
      scale: {
        scale: "",
        folder: "",
        scaleData: {},
        scalesInfo: {
          "TANITA BC-601": "image.jpeg",
        },
      },
    },
  });

  return render(
    <Provider store={fakeStore}>
      <HashRouter>{ui}</HashRouter>
    </Provider>
  );
};

jest.mock("../../../wailsjs/go/main/App", () => ({
  ReadFolder: jest.fn(() => ({ DATA: "data", SYSTEM: "system" })),
  GetFolderDialog: jest.fn(() => "folder"),
}));

describe("<ReadScaleData />", () => {
  it("Should render", () => {
    customRender(<ReadScaleData />);

    expect(
      screen.getByText("read-scale-data.choose-scale")
    ).toBeInTheDocument();
    expect(screen.getByText("read-scale-data.choose")).toBeInTheDocument();
    expect(screen.getByText("read-scale-data.read-data")).toBeInTheDocument();
  });

  it("Should open file dialog on button click", async () => {
    customRender(<ReadScaleData />);

    const btn = screen.getByText("read-scale-data.choose");
    fireEvent.click(btn);

    await waitFor(() => {
      expect(screen.getByText(/folder/)).toBeInTheDocument();
    });

    expect(fakeStore.getState().scale.folder).toBe("folder");
  });

  it("Should submit", () => {
    customRender(<ReadScaleData />);

    const { scale, folder } = fakeStore.getState().scale;

    const btn = screen.getByRole("button", {
      name: "read-scale-data.read-data",
    });
    fireEvent.click(btn);

    expect(ReadFolder).toHaveBeenCalledWith(scale, folder);
  });
});
