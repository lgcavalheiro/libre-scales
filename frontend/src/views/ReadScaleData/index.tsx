import {
  Button,
  Input,
  MenuItem,
  Select,
  type SelectChangeEvent,
  Card,
  CardMedia,
  Box,
  FormControl,
  FormHelperText,
  InputLabel,
} from "@mui/material";
import { GetFolderDialog, ReadFolder } from "../../../wailsjs/go/main/App";
import scaleQuestion from "../../assets/scale-question.webp";
import { useDispatch, useSelector } from "react-redux";
import {
  selectFolder,
  selectScale,
  selectScalesInfo,
  setFolder,
  setScale,
  setScaleData,
} from "../../store/features/scaleSlice";
import { useTranslation } from "react-i18next";

const ReadScaleData = (): JSX.Element => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const scale = useSelector(selectScale);
  const folder = useSelector(selectFolder);
  const knownScales = useSelector(selectScalesInfo);

  const handleFolderDialog = async (): Promise<void> => {
    const selectedFolder = await GetFolderDialog();
    dispatch(setFolder(selectedFolder));
  };

  const handleSelect = (event: SelectChangeEvent): void => {
    dispatch(setScale(event.target.value));
  };

  const handleSubmit = async (): Promise<void> => {
    const result = await ReadFolder(scale, folder);
    dispatch(setScaleData(result));
    window.location.hash = "#/data/view";
  };

  return (
    <Card elevation={3} sx={{ minHeight: 320, display: "flex" }}>
      <CardMedia
        component="img"
        src={
          knownScales[scale] != null
            ? `/src/assets/${knownScales[scale]}`
            : scaleQuestion
        }
        sx={{ width: 320 }}
      />
      <Box sx={{ display: "grid", padding: 6, width: "100%" }}>
        <FormControl>
          <InputLabel>{t("read-scale-data.choose-scale")}</InputLabel>
          <Select
            onChange={handleSelect}
            variant="standard"
            defaultValue=""
            data-testid="scale-select"
          >
            {Object.keys(knownScales).map((scaleName: string) => (
              <MenuItem value={scaleName} key={scaleName}>
                {scaleName}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl sx={{ margin: "32px 0px" }}>
          <InputLabel>{t("read-scale-data.choose-file-folder")}</InputLabel>
          <Input value={folder ?? ""} readOnly />
          {knownScales[scale] != null && (
            <FormHelperText>
              {t(`read-scale-data.folder-path.${scale}`)}
            </FormHelperText>
          )}
          <Button
            onClick={() => {
              void handleFolderDialog();
            }}
            sx={{ placeSelf: "start" }}
            size="large"
          >
            {t("read-scale-data.choose")}
          </Button>
        </FormControl>

        <Button
          onClick={() => {
            void handleSubmit();
          }}
          size="large"
          variant="contained"
          sx={{ placeSelf: "end" }}
        >
          {t("read-scale-data.read-data")}
        </Button>
      </Box>
    </Card>
  );
};

export default ReadScaleData;
