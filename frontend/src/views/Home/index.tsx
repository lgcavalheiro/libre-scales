import { Button, Grid, Paper, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

const Home = (): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Paper
      elevation={3}
      sx={{
        padding: 6,
        display: "grid",
        justifyContent: "center",
      }}
    >
      <Typography variant="h3">{t("home.welcome")}</Typography>
      <Typography variant="h4">{t("home.what-would")}</Typography>
      <Grid container sx={{ marginTop: 8 }} columnSpacing={4}>
        <Grid item>
          <Button
            component={Link}
            to="/client/register"
            size="large"
            disableElevation
            variant="contained"
          >
            {t("home.register-client")}
          </Button>
        </Grid>
        <Grid item>
          <Button
            component={Link}
            to="/client/view"
            size="large"
            disableElevation
            variant="contained"
          >
            {t("home.view-clients")}
          </Button>
        </Grid>
        <Grid item>
          <Button
            component={Link}
            to="/data/read"
            size="large"
            disableElevation
            variant="contained"
          >
            {t("home.read-scale-data")}
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default Home;
