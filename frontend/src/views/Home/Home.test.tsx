import {
  screen,
  render,
  fireEvent,
  type RenderResult,
} from "@testing-library/react";
import Home from ".";
import { type ReactNode } from "react";
import { HashRouter } from "react-router-dom";

const customRender = (ui: ReactNode): RenderResult =>
  render(<HashRouter>{ui}</HashRouter>);

describe("<Home />", () => {
  it("Should render", () => {
    customRender(<Home />);

    expect(screen.getByText("home.welcome")).toBeInTheDocument();
    expect(screen.getByText("home.what-would")).toBeInTheDocument();
    expect(screen.getByText("home.register-client")).toBeInTheDocument();
    expect(screen.getByText("home.view-clients")).toBeInTheDocument();
    expect(screen.getByText("home.read-scale-data")).toBeInTheDocument();
  });

  it.each([
    ["home.register-client", "#/client/register"],
    ["home.view-clients", "#/client/view"],
    ["home.read-scale-data", "#/data/read"],
  ])(
    "When button %p is clicked, user should be taken to view with text %p",
    (btnText, route) => {
      customRender(<Home />);

      const btn = screen.getByText(btnText);
      fireEvent.click(btn);

      expect(window.location.hash).toBe(route);
    }
  );
});
