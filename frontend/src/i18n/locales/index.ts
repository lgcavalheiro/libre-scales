import pt_br from "./pt-br.json";
import en_us from "./en-us.json";
import { type Resource } from "i18next";

const resources: Resource = {
  "en-US": { translation: en_us },
  "pt-BR": { translation: pt_br },
};

export const knownLanguages = Object.keys(resources);

export default resources;
