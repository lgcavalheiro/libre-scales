import "@testing-library/jest-dom";

jest.mock("react-i18next", () => ({
  useTranslation: () => ({
    t: (key: string) => key,
    i18n: {
      language: "english",
    },
  }),
}));

jest.mock("./wailsjs/go/main/App", () => ({
  GetFolderDialog: () => "folder",
  ReadFolder: (scale: string, folder: string) => ({
    scale,
    folder,
  }),
}));

jest.mock("./wailsjs/go/scaleresolvers/ScaleResolver", () => ({
  GetScaleImages: () => ({
    "TANITA BC-601": "image.jpeg",
  }),
}));

jest.mock("@mui/x-charts", () => ({
  BarChart: jest.fn().mockImplementation(({ children }) => children),
  LineChart: jest.fn().mockImplementation(({ children }) => children),
}));
