export const preloadedSingleEntryState = {
  report: {
    rawData: [],
    reportData: {
      generalInfo: {
        age: 25,
        gender: "Male",
        type: "Type A",
        height: 180,
        activityLevel: 3,
      },
      fatInfo: [
        {
          date: "2022-01-01T00:00:00.000Z",
          globalPercentage: 15,
          globalMass: 20,
          torso: {
            percentage: 10,
            mass: 12,
          },
          leftArm: {
            percentage: 5,
            mass: 6,
          },
          rightArm: {
            percentage: 8,
            mass: 10,
          },
          leftLeg: {
            percentage: 7,
            mass: 8,
          },
          rightLeg: {
            percentage: 9,
            mass: 11,
          },
        },
      ],
      miscInfo: [
        {
          weight: 70,
          bmi: 22,
          visceralFat: 8,
          metabolicAge: 30,
          bodyWater: 60,
          boneMass: 3,
          calorieIntake: 2000,
          date: "2022-01-01T00:00:00.000Z",
        },
      ],
      muscleInfo: [
        {
          date: "2022-01-01T00:00:00.000Z",
          globalPercentage: 15,
          globalMass: 20,
          torso: {
            percentage: 10,
            mass: 12,
          },
          leftArm: {
            percentage: 5,
            mass: 6,
          },
          rightArm: {
            percentage: 8,
            mass: 10,
          },
          leftLeg: {
            percentage: 7,
            mass: 8,
          },
          rightLeg: {
            percentage: 9,
            mass: 11,
          },
        },
      ],
    },
  },
};

export const preloadedMultiEntryState = {
  report: {
    rawData: [],
    reportData: {
      generalInfo: {
        age: 42,
        gender: "Female",
        type: "Type A",
        height: 158,
        activityLevel: 1,
      },
      fatInfo: [
        {
          date: "2022-01-01T00:00:00.000Z",
          globalPercentage: 15,
          globalMass: 20,
          torso: {
            percentage: 10,
            mass: 12,
          },
          leftArm: {
            percentage: 5,
            mass: 6,
          },
          rightArm: {
            percentage: 8,
            mass: 10,
          },
          leftLeg: {
            percentage: 7,
            mass: 8,
          },
          rightLeg: {
            percentage: 9,
            mass: 11,
          },
        },
        {
          date: "2022-02-11T00:00:00.000Z",
          globalPercentage: 13,
          globalMass: 21,
          torso: {
            percentage: 12,
            mass: 12,
          },
          leftArm: {
            percentage: 4,
            mass: 6,
          },
          rightArm: {
            percentage: 9,
            mass: 10,
          },
          leftLeg: {
            percentage: 7,
            mass: 8,
          },
          rightLeg: {
            percentage: 7,
            mass: 11,
          },
        },
      ],
      miscInfo: [
        {
          weight: 70,
          bmi: 22,
          visceralFat: 8,
          metabolicAge: 30,
          bodyWater: 60,
          boneMass: 3,
          calorieIntake: 2000,
          date: "2022-01-01T00:00:00.000Z",
        },
        {
          weight: 67.8,
          bmi: 21,
          visceralFat: 7,
          metabolicAge: 29,
          bodyWater: 64,
          boneMass: 3.5,
          calorieIntake: 2120,
          date: "2022-02-11T00:00:00.000Z",
        },
      ],
      muscleInfo: [
        {
          date: "2022-01-01T00:00:00.000Z",
          globalPercentage: 15,
          globalMass: 20,
          torso: {
            percentage: 10,
            mass: 12,
          },
          leftArm: {
            percentage: 5,
            mass: 6,
          },
          rightArm: {
            percentage: 8,
            mass: 10,
          },
          leftLeg: {
            percentage: 7,
            mass: 8,
          },
          rightLeg: {
            percentage: 9,
            mass: 11,
          },
        },
        {
          date: "2022-02-11T00:00:00.000Z",
          globalPercentage: 18,
          globalMass: 22,
          torso: {
            percentage: 9,
            mass: 12,
          },
          leftArm: {
            percentage: 7,
            mass: 6,
          },
          rightArm: {
            percentage: 3,
            mass: 10,
          },
          leftLeg: {
            percentage: 9,
            mass: 8,
          },
          rightLeg: {
            percentage: 8,
            mass: 11,
          },
        },
      ],
    },
  },
};
