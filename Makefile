COVER=coverage

# fcmd === frontend command
fcmd:
	cd frontend && npm ${c}

go-test:
	$(shell [ ! -e ${COVER} ] && mkdir ${COVER})
	go test ./... -v -race -covermode=atomic -coverprofile=${COVER}/coverage.out
	go tool cover -html=${COVER}/${COVER}.out -o ${COVER}/${COVER}.html