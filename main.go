package main

import (
	"embed"

	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
	"github.com/wailsapp/wails/v2/pkg/runtime"
	SR "gitlab.com/lgcavalheiro/libre-scales/scaleResolvers"
	"golang.org/x/exp/slices"
)

//go:embed all:frontend/dist
var assets embed.FS

var scaleResolver = SR.NewScaleResolver()

func (a *App) GetFolderDialog() string {
	opts := runtime.OpenDialogOptions{
		DefaultDirectory: ".",
		// DefaultFilename:  "",
		// Title: "Choose a folder",
		// Filters: [],
		ShowHiddenFiles:            false,
		CanCreateDirectories:       false,
		ResolvesAliases:            false,
		TreatPackagesAsDirectories: false,
	}
	selected, err := runtime.OpenDirectoryDialog(a.ctx, opts)

	if err != nil {
		runtime.LogError(a.ctx, err.Error())
	}

	return selected
}

func (a *App) ReadFolder(scale, dirname string) (interface{}, error) {
	if !slices.Contains(scaleResolver.GetScaleNames(), scale) {
		runtime.LogError(a.ctx, "Scale not supported!")
	}

	return scaleResolver.Scales[scale].Resolver(dirname)
}

func main() {
	// Create an instance of the app structure
	app := NewApp()

	// Create application with options
	err := wails.Run(&options.App{
		Title:  "libre-scales",
		Width:  1024,
		Height: 768,
		AssetServer: &assetserver.Options{
			Assets: assets,
		},
		BackgroundColour: &options.RGBA{R: 27, G: 38, B: 54, A: 1},
		OnStartup:        app.startup,
		Bind: []interface{}{
			app,
			scaleResolver,
		},
	})

	if err != nil {
		println("Error:", err.Error())
	}
}
