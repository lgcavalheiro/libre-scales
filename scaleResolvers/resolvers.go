package scaleresolvers

import (
	"path/filepath"
	"reflect"
)

type ScaleResolver struct {
	Scales map[string]ScaleInfo
}

type DirWalker = func(string, filepath.WalkFunc) error

type ScaleInfo struct {
	Image    string
	Resolver func(string) (interface{}, error)
}

func NewScaleResolver() *ScaleResolver {
	scales := map[string]ScaleInfo{
		"TANITA BC-601": {
			Image:    "model-bc-601.jpg",
			Resolver: ResolveTanitaBC601,
		},
	}

	return &ScaleResolver{
		Scales: scales,
	}
}

func (SR *ScaleResolver) GetScaleImages() map[string]string {
	images := map[string]string{}

	for scale := range SR.Scales {
		images[scale] = SR.Scales[scale].Image
	}

	return images
}

func (SR *ScaleResolver) GetScaleNames() []string {
	mapKeys := reflect.ValueOf(SR.Scales).MapKeys()
	keys := []string{}

	for k := range mapKeys {
		keys = append(keys, mapKeys[k].Interface().(string))
	}

	return keys
}
