package scaleresolvers

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type TanitaTestSuite struct {
	suite.Suite
	rootDir              string
	fileName             string
	fileContent          string
	perm                 fs.FileMode
	triggerErrorFileName string
	fileNotFoundName     string
	noNumberFileName     string
}

func (s *TanitaTestSuite) SetupSuite() {
	s.rootDir = "/tmp/librescalestest"
	s.fileName = "DATA1.CSV"
	s.fileContent = "{mock,file,content,1}"
	s.perm = 0755
	s.triggerErrorFileName = "triggerError"
	s.fileNotFoundName = "notFound.CSV"
	s.noNumberFileName = "NONUMBER.CSV"

	os.Mkdir(s.rootDir, s.perm)
	os.MkdirAll(path.Join(s.rootDir, "GRAPHV1/SYSTEM"), s.perm)
	os.MkdirAll(path.Join(s.rootDir, "GRAPHV1/DATA"), s.perm)
}

func (s *TanitaTestSuite) TearDownSuite() {
	os.RemoveAll(s.rootDir)
}

func (s *TanitaTestSuite) MockDirWalker(filepath string, visit filepath.WalkFunc) error {
	testfile := path.Join(s.rootDir, s.fileName)
	os.WriteFile(testfile, []byte(s.fileContent), s.perm)
	fileinfo, _ := os.Stat(testfile)

	switch filepath {
	case s.rootDir:
		return visit(testfile, fileinfo, nil)
	case s.triggerErrorFileName:
		return visit(testfile, fileinfo, errors.New("test error"))
	case s.fileNotFoundName:
		return visit(s.fileNotFoundName, fileinfo, nil)
	case s.noNumberFileName:
		testfile := path.Join(s.rootDir, s.noNumberFileName)
		os.WriteFile(testfile, []byte(s.fileContent), s.perm)
		fileinfo, _ := os.Stat(testfile)
		return visit(testfile, fileinfo, nil)
	}

	return visit(testfile, fileinfo, nil)
}

func (s *TanitaTestSuite) TestReadDataDir() {
	res, err := readDataDir(s.rootDir, s.MockDirWalker)

	assert.Nil(s.T(), err)
	assert.Equal(s.T(), s.fileContent, res[1])
}

func (s *TanitaTestSuite) TestReadDataDirReturnsErrIfProvided() {
	_, err := readDataDir(s.triggerErrorFileName, s.MockDirWalker)

	assert.NotNil(s.T(), err)
	assert.Equal(s.T(), "test error", err.Error())
}

func (s *TanitaTestSuite) TestReadDataDirReturnsErrIfFileNotFound() {
	_, err := readDataDir(s.fileNotFoundName, s.MockDirWalker)

	assert.NotNil(s.T(), err)
	assert.Equal(s.T(), fmt.Sprintf("open %s: no such file or directory", s.fileNotFoundName), err.Error())
}

func (s *TanitaTestSuite) TestReadDataDirReturnsErrIfNumberParsingFails() {
	_, err := readDataDir(s.noNumberFileName, s.MockDirWalker)

	assert.NotNil(s.T(), err)
	assert.Equal(s.T(), "strconv.Atoi: parsing \"\": invalid syntax", err.Error())
}

func (s *TanitaTestSuite) TestResolveTanitaBC601() {
	res, err := ResolveTanitaBC601(s.rootDir)

	expected := map[string]map[int]string{
		"DATA":   {},
		"SYSTEM": {},
	}

	assert.Nil(s.T(), err)
	assert.Equal(s.T(), expected, res)
}

func (s *TanitaTestSuite) TestResolveTanitaBC601ReturnsErr() {
	invalidDir := path.Join(s.rootDir, "invalidDir")
	res, err := ResolveTanitaBC601(invalidDir)

	assert.Nil(s.T(), res)
	assert.Equal(s.T(), fmt.Sprintf("lstat %s/GRAPHV1/SYSTEM: no such file or directory", invalidDir), err.Error())
}

func TestTanitaTestSuite(t *testing.T) {
	suite.Run(t, new(TanitaTestSuite))
}
