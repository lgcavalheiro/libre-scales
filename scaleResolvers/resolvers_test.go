package scaleresolvers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type ResolversTestSuite struct {
	suite.Suite
	ScaleResolver *ScaleResolver
}

func (s *ResolversTestSuite) SetupSuite() {
	s.ScaleResolver = NewScaleResolver()
}

func (s *ResolversTestSuite) TestGetScaleImages() {
	images := s.ScaleResolver.GetScaleImages()

	assert.NotEmpty(s.T(), images)
}

func (s *ResolversTestSuite) TestGetScaleNames() {
	names := s.ScaleResolver.GetScaleNames()

	assert.NotEmpty(s.T(), names)
}

func TestResolversTestSuite(t *testing.T) {
	suite.Run(t, new(ResolversTestSuite))
}
