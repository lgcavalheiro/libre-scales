package scaleresolvers

import (
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

func readDataDir(dirname string, walk DirWalker) (map[int]string, error) {
	files := map[int]string{}

	err := walk(dirname, func(filepath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() && strings.Contains(filepath, ".CSV") {
			bContent, err := os.ReadFile(filepath)
			if err != nil {
				return err
			}

			re := regexp.MustCompile("[0-9]")
			index, err := strconv.Atoi(strings.Join(re.FindStringSubmatch(info.Name()), ""))
			if err != nil {
				return err
			}

			content := strings.TrimSpace(string(bContent))
			if len(content) > 0 {
				files[index] = content
			}
		}

		return nil
	})

	return files, err
}

func ResolveTanitaBC601(dirname string) (interface{}, error) {
	if !strings.Contains(dirname, "GRAPHV1") {
		dirname = path.Join(dirname, "GRAPHV1")
	}

	result := map[string]map[int]string{}
	dirs := []string{"SYSTEM", "DATA"}

	for _, dir := range dirs {
		data, err := readDataDir(path.Join(dirname, dir), filepath.Walk)

		if err != nil {
			return nil, err
		}

		result[dir] = data
	}

	return result, nil
}
